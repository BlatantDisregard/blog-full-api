describe('Numeric addition', () => {
  it('sums integers correctly', () => {
    expect(2 + 3).toEqual(5);
  });
});
